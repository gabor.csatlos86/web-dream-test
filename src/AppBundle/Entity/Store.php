<?php
namespace AppBundle\Entity;

use AppBundle\Model\Item;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\ProductInStore;

/**
 * @author Gabor Csatlos <gabor.csatlos86@gmail.com>
 */
class Store extends Item {
	
	/**
	 * @var int
	 */
	protected $id;
	
	/**
	 * @var string
	 */
	protected $storeName;
	
	/**
	 * @var string
	 */
	protected $address;
	
	/**
	 * @var int
	 */
	protected $capacity;
	
	/**
	 * @var ArrayCollection
	 */
	protected $products;
	
	
	public function __construct()
	{
		$this->products = new ArrayCollection();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \AppBundle\Model\ItemInterface::getId()
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \AppBundle\Model\ItemInterface::setId()
	 */
	public function setId($id)
	{
		$this->id = $id;
	}
	
	/**
	 * @return string
	 */
	public function getStoreName()
	{
		return $this->storeName;
	}
	
	/**
	 * @param string $storeName
	 */
	public function setStoreName($storeName)
	{
		$this->storeName = $storeName;
	}
	
	/**
	 * @return string
	 */
	public function getAddress()
	{
		return $this->address;
	}
	
	/**
	 * @param string $address
	 */
	public function setAddress($address)
	{
		$this->address = $address;
	}
	
	/**
	 * @return number
	 */
	public function getCapacity()
	{
		return $this->capacity;
	}
	
	/**
	 * @param int $capacity
	 */
	public function setCapacity($capacity)
	{
		$this->capacity = $capacity;
	}
	
	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getProducts()
	{
		return $this->products;
	}
	
	/**
	 * @param ArrayCollection $products
	 */
	public function setProducts(ArrayCollection $products)
	{
		$this->products = $products;
	}
	
	/**
	 * @param ProductInStore $productInStore
	 */
	public function addProduct(ProductInStore $productInStore)
	{
		$this->products->add($productInStore);
	}
	
	/**
	 * @param ProductInStore $productInStore
	 */
	public function removeProduct(ProductInStore $productInStore)
	{
		if($this->products->contains($productInStore)){
			$this->products->removeElement($productInStore);
		}
	}
	
	/**
	 * @return number
	 */
	public function getSumProductNum()
	{
		$sum = 0;
		foreach ($this->products as $productInStore) {
			$sum += $productInStore->getProductNum();
		}
		return $sum;
	}
	
	/**
	 * @return string;
	 */
	public function getStoredProducts()
	{
		$itemData = array();
		foreach ($this->products as $productInStore) {
			$itemData[] = $productInStore->__toString();
		}
		return implode(' | ', $itemData);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \AppBundle\Model\ItemInterface::__toString()
	 */
	public function __toString()
	{
		return $this->getStoreName() ?: '';
	}
	
}