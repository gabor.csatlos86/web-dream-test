<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Product;
use AppBundle\Entity\Store;

/**
 * @author Gabor Csatlos <gabor.csatlos86@gmail.com>
 */
class ProductInStore {
	
	/**
	 * @var int
	 */
	protected $id;
	
	/**
	 * @var Product
	 */
	protected $product;
	
	/**
	 * @var Store
	 */
	protected $store;
	
	/**
	 * @var int
	 */
	protected $productNum;
	
	
	/**
	 * @return number
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}
	
	/**
	 * @return \AppBundle\Entity\Product
	 */
	public function getProduct()
	{
		return $this->product;
	}
	
	/**
	 * @param Product $product
	 */
	public function setProduct(Product $product)
	{
		$this->product = $product;
	}
	
	/**
	 * @return \AppBundle\Entity\Store
	 */
	public function getStore()
	{
		return $this->store;
	}
	
	/**
	 * @param Store $store
	 */
	public function setStore(Store $store)
	{
		$this->store = $store;
	}
	
	/**
	 * @return number
	 */
	public function getProductNum()
	{
		return $this->productNum;
	}
	
	/**
	 * @param int $productNum
	 */
	public function setProductNum($productNum)
	{
		$this->productNum = $productNum;
	}
	
	public function __toString()
	{
		return $this->product ? $this->product->getProductName() . ' ( '. $this->product->getProductNumber() . ' ) : '. $this->productNum : '';
	}
	
}