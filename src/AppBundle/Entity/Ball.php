<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Product;

/**
 * @author Gabor Csatlos <gabor.csatlos86@gmail.com>
 */
class Ball extends Product {
	
	/**
	 * @var string
	 */
	protected $diameter;
	
	
	/**
	 * @return string
	 */
	public function getDiameter()
	{
		return $this->diameter;
	}
	
	/**
	 * @param string $diameter
	 */
	public function setDiameter($diameter)
	{
		$this->diameter = $diameter;
	}
	
}