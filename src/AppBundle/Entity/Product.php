<?php

namespace AppBundle\Entity;

use AppBundle\Model\Item;
use AppBundle\Entity\Brand;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\ProductInStore;

/**
 * @author Gabor Csatlos <gabor.csatlos86@gmail.com>
 */
class Product extends Item {
	
	/**
	 * @var int
	 */
	protected $id;
	
	/**
	 * @var string
	 */
	protected $productName;
	
	/**
	 * @var string
	 */
	protected $productNumber;
	
	/**
	 * @var string
	 */
	protected $price;
	
	/**
	 * @var Brand
	 */
	protected $brand;
	
	/**
	 * @var ArrayCollection
	 */
	protected $stores;
	
	
	public function __construct()
	{
		$this->stores = new ArrayCollection();
	}
	
	/**
	 * {@inheritdoc}
	 * @see \AppBundle\Model\ItemInterface::getId()
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \AppBundle\Model\ItemInterface::setId()
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getProductName()
	{
		return $this->productName;
	}
	
	/**
	 * @param string $productName
	 */
	public function setProductName($productName)
	{
		$this->productName = $productName;
	}
	
	/**
	 * @return string
	 */
	public function getProductNumber()
	{
		return $this->productNumber;
	}
	
	/**
	 * @param string $productNumber
	 */
	public function setProductNumber($productNumber)
	{
		$this->productNumber = $productNumber;
	}
	
	/**
	 * @return string
	 */
	public function getPrice()
	{
		return $this->price;
	}
	
	/**
	 * @param string $price
	 */
	public function setPrice($price)
	{
		$this->price = $price;
	}
	
	/**
	 * @return \AppBundle\Entity\Brand
	 */
	public function getBrand()
	{
		return $this->brand;
	}
	
	/**
	 * @param Brand $brand
	 */
	public function setBrand(Brand $brand)
	{
		$this->brand = $brand;
	}
	
	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getStores()
	{
		return $this->stores;
	}
	
	/**
	 * @param ArrayCollection $stores
	 */
	public function setStores(ArrayCollection $stores)
	{
		$this->stores = $stores;
	}
	
	/**
	 * @param ProductInStore $productInStore
	 */
	public function addProductToStore(ProductInStore $productInStore)
	{
		$this->stores->add($productInStore);
	}
	
	/**
	 * @param ProductInStore $productInStore
	 */
	public function removeProductFromStore(ProductInStore $productInStore)
	{
		if($this->stores->contains($productInStore)){
			$this->stores->removeElement($productInStore);
		}
	}
	
	/**
	 * @return number
	 */
	public function getUsedStoreNum()
	{
		return $this->stores->count();
	}
	
	/**
	 * {@inheritdoc}
	 * @see \AppBundle\Model\ItemInterface::__toString()
	 */
	public function __toString()
	{
		return $this->getProductName() ?: '';
	}
}