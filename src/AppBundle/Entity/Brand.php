<?php
namespace AppBundle\Entity;

use AppBundle\Model\Item;
use AppBundle\Enum\QualityCategoryEnum;

/**
 * @author Gabor Csatlos <gabor.csatlos86@gmail.com>
 */
class Brand extends Item {
	
	/**
	 * @var int
	 */
	protected $id;
	
	/**
	 * @var string
	 */
	protected $brandName;
	
	/**
	 * @var int
	 */
	protected $qualityCategory = QualityCategoryEnum::CATEGORY_NO_VALUE;
	
	/**
	 * {@inheritDoc}
	 * @see \AppBundle\Model\ItemInterface::getId()
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \AppBundle\Model\ItemInterface::setId()
	 */
	public function setId($id)
	{
		$this->id = $id;
	}
	
	/**
	 * @return string
	 */
	public function getBrandName()
	{
		return $this->brandName;
	}
	
	/**
	 * @param unknown $brandName
	 */
	public function setBrandName($brandName)
	{
		$this->brandName = $brandName;
	}
	
	/**
	 * @return number
	 */
	public function getQualityCategory()
	{
		return $this->qualityCategory;
	}
	
	/**
	 * @param int $qualityCategory
	 */
	public function setQualityCategory($qualityCategory)
	{
		$this->qualityCategory = $qualityCategory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \AppBundle\Model\ItemInterface::__toString()
	 */
	public function __toString()
	{
		return $this->getBrandName() ?: '';
	}
}