<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Product;

/**
 * @author Gabor Csatlos <gabor.csatlos86@gmail.com>
 */
class Box extends Product {
	
	/**
	 * @var string
	 */
	protected $sizeWidth;
	
	/**
	 * @var string
	 */
	protected $sizeHeight;
	
	/**
	 * @var string
	 */
	protected $sizeDepth;
	
	
	/**
	 * @return string
	 */
	public function getSizeWidth()
	{
		return $this->sizeWidth;
	}
	
	/**
	 * @param string $sizeWidth
	 */
	public function setSizeWidth($sizeWidth)
	{
		$this->sizeWidth = $sizeWidth;
	}
	
	/**
	 * @return string
	 */
	public function getSizeHeight()
	{
		return $this->sizeHeight;
	}
	
	/**
	 * @param string $sizeHeight
	 */
	public function setSizeHeight($sizeHeight)
	{
		$this->sizeHeight = $sizeHeight;
	}
	
	/**
	 * @return string
	 */
	public function getSizeDepth()
	{
		return $this->sizeDepth;
	}
	
	/**
	 * @param string $sizeDepth
	 */
	public function setSizeDepth($sizeDepth)
	{
		$this->sizeDepth = $sizeDepth;
	}
}