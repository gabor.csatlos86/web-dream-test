<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use AppBundle\Entity\Ball;
use AppBundle\Entity\Box;

class ProductAdmin extends AbstractAdmin
{
	protected function configureFormFields(FormMapper $formMapper)
	{
		$subject = $this->getSubject();
		
		$formMapper->add('productName', 'text')
			->add('productNumber', 'text')
			->add('price', 'text')
			->add('brand','entity', array('class' => 'AppBundle\Entity\Brand', 'query_builder' => null) );
		
		if($subject instanceof Ball){
			$formMapper->add('diameter', 'text');
		}
		
		if($subject instanceof Box){
			$formMapper->add('sizeWidth', 'text')
				->add('sizeHeight', 'text')
				->add('sizeDepth', 'text');
		}
	}
	
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper->add('productName');
	}
	
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper->addIdentifier('productName')
			->add('productNumber')
			->add('price')
			->add('brand');
	}
}
