<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use AppBundle\Entity\Store;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductInStore;

class StoreAdmin extends AbstractAdmin
{
	protected function configureRoutes(RouteCollection $collection)
	{
		$collection->add('addProductToStore', 'add-product-to-store');
		$collection->add('removeProductFromStore', 'remove-product-from-store');
	}
	
	protected function configureFormFields(FormMapper $formMapper)
	{
		$subject = $this->getSubject();
		
		if($subject instanceof Store) {
			$formMapper->add('storeName', 'text')
				->add('address', 'text')
				->add('capacity', 'number');
		}
		
		if($subject instanceof ProductInStore) {
			$formMapper->add('product','entity', array('class' => 'AppBundle\Entity\Product', 'query_builder' => null) )
				->add('store','entity', array('class' => 'AppBundle\Entity\Store', 'query_builder' => null) )
				->add('productNum', 'number');
		}
	}
	
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper->add('storeName');
	}
	
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper->addIdentifier('storeName')
			->add('address')
			->add('capacity')
			->add('sumProductNum')
			->add('storedProducts');
	}
	
	public function configureActionButtons($action, $object = null)
	{
		$list = parent::configureActionButtons($action, $object);
		$list['add_product_to_store'] = array(
				'template' => 'AppBundle:Button:add_product_to_store_button.html.twig',
		);
		$list['remove_product_from_store'] = array(
				'template' => 'AppBundle:Button:remove_product_from_store_button.html.twig',
		);
		return $list;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function getFormBuilder()
	{
		$this->formOptions['csrf_protection'] = false;
		$this->formOptions['allow_extra_fields'] = true;
		return parent::getFormBuilder();
	}
}
