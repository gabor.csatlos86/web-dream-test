<?php

namespace AppBundle\Enum;

/**
 * @author Gabor Csatlos <gabor.csatlos86@gmail.com>
 */
class QualityCategoryEnum {
	
	
	const CATEGORY_NO_VALUE = 0;
	
	const CATEGORY_BAD = 1;
	
	const CATEGORY_CHEAP = 2;
	
	const CATEGORY_MEDIUM = 3;
	
	CONST CATEGORY_GOOD = 4;
	
	const CATEGORY_PERFECT = 5;
	
	public static function getAvailableValues(){
		return array(
				self::CATEGORY_NO_VALUE,
				self::CATEGORY_BAD,
				self::CATEGORY_CHEAP,
				self::CATEGORY_MEDIUM,
				self::CATEGORY_GOOD,
				self::CATEGORY_PERFECT
		);
	}
	
	/**
	 * @return array
	 */
	public static function getEnumValues() {
		return array(
				'1' => '1',
				'2' => '2',
				'3' => '3',
				'4' => '4',
				'5' => '5',
		);
	}
}