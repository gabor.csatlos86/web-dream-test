<?php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use AppBundle\Entity\ProductInStore;
use AppBundle\Entity\Product;
use AppBundle\Entity\Store;
use Symfony\Component\HttpFoundation\RedirectResponse;

class StoreCRUDController extends CRUDController
{
	
	public function addProductToStoreAction()
	{
		$request = $this->getRequest();
		$object = new ProductInStore();
		$this->admin->setSubject($object);
		
		/** @var $form \Symfony\Component\Form\Form */
		$form = $this->admin->getForm();
		//$form->setData($object);
		$form->handleRequest($request);
		
		if ($form->isSubmitted()) {
			if($form->isValid()){
				$formData = $form->getData();
				$productNum = $formData->getproductNum();
				/** @var Store $store */
				$store = $formData->getStore();
				/** @var Product $product */
				$product = $formData->getProduct();
				
				$allStore = $this->getAllStore();
				$otherStore = $this->removeStoreFromList($store, $allStore);
				$res = $this->storeProduct($product, $store, $productNum, $otherStore);
				if(is_numeric($res)){
					$this->addFlash('sonata_flash_error', 'Can\'t store '.$res);
				}
				$url = $this->admin->generateUrl('list');
				return new RedirectResponse($url);
			}
		}
		
		$formView = $form->createView();
		
		return $this->render('AppBundle:CRUD:add_product_to_store.html.twig', array(
				'action' => 'create',
				'form' => $formView,
				'object' => $object,
		), null);
	}
	
	public function removeProductFromStoreAction()
	{
		$request = $this->getRequest();
		$object = new ProductInStore();
		$this->admin->setSubject($object);
		
		/** @var $form \Symfony\Component\Form\Form */
		$form = $this->admin->getForm();
		$form->setData($object);
		$form->handleRequest($request);
		
		if ($form->isSubmitted()) {
			if($form->isValid()){
				$formData = $form->getData();
				$productNum = $formData->getproductNum();
				/** @var Store $store */
				$store = $formData->getStore();
				/** @var Product $product */
				$product = $formData->getProduct();
				
				$res = $this->removeProduct($product, $store, $productNum);
				if(is_numeric($res)){
					$this->addFlash('sonata_flash_error', 'Can\'t remove '.$res);
				}
				$url = $this->admin->generateUrl('list');
				return new RedirectResponse($url);
			}
		}
		
		$formView = $form->createView();
		
		return $this->render('AppBundle:CRUD:remove_product_from_store.html.twig', array(
				'action' => 'create',
				'form' => $formView,
				'object' => $object,
		), null);
	}
	
	
	protected function storeProduct(Product $product, Store $store, $productNum, $otherStore = array() )
	{
		$oldProductInStore = $this->loadProductIntStore($product, $store);
		$oldsNum = count($oldProductInStore);
		
		if($oldsNum > 1) {
			throw new \Exception();
		}
		$pluss = 0;
		$object = new ProductInStore();
		$em = $this->admin->getModelManager()->getEntityManager($object);
		
		if($oldsNum == 0) {
			if($productNum > ( $store->getCapacity() - $store->getSumProductNum() )) {
				$pluss = $productNum - ( $store->getCapacity() - $store->getSumProductNum());
				
			}
			
			$object->setProduct($product);
			$object->setStore($store);
			$object->setProductNum(($productNum - $pluss));
			$em->persist($object);
			
		} else {
			if( $productNum > ( $store->getCapacity() - $store->getSumProductNum() ) ) {
				$pluss = $productNum - ( $store->getCapacity() - $store->getSumProductNum());
			}
			$oldProductNum = $oldProductInStore[0]->getProductNum();
			$oldProductInStore[0]->setProductNum( $oldProductNum + ($productNum - $pluss));
			$this->admin->update($oldProductInStore[0]);
		}
		
		$em->flush();
		
		if($pluss > 0) {
			if(empty($otherStore)) {
				return $pluss;
			}
			$nextStore = false;
			foreach ($otherStore as $item){
				$nextStore = $item;
				break;
			}
			$otherStore = $this->removeStoreFromList($nextStore, $otherStore);
			
			return $this->storeProduct($product, $nextStore, $pluss, $otherStore);
		}
		return true;
	}
	
	protected function removeProduct(Product $product, Store $store, $productNum)
	{
		$oldProductInStore = $this->loadProductIntStore($product, $store);
		$oldsNum = count($oldProductInStore);
		
		if($oldsNum > 1) {
			throw new \Exception();
		}
		
		$pluss = 0;
		$object = new ProductInStore();
		$em = $this->admin->getModelManager()->getEntityManager($object);
		
		if($oldsNum == 0) {
			if($product->getStores()->first() instanceof ProductInStore && $nextStore = $product->getStores()->first()->getStore() instanceof Store) {
				$nextStore = $product->getStores()->first()->getStore();
				return $this->removeProduct($product, $nextStore, $productNum);
			}
			return $productNum;
		}
		$oldProductNum = $oldProductInStore[0]->getProductNum();
		if( $productNum <= $oldProductNum) {
			$oldProductInStore[0]->setProductNum( ($oldProductNum - $productNum ) );
			$em->flush();
			return true;
		}
		
		$pluss = $productNum - $oldProductNum;
		$product->getStores()->removeElement($oldProductInStore[0]);
		$em->remove($oldProductInStore[0]);
		
		$em->flush();
		if($product->getStores()->first() instanceof ProductInStore && $nextStore = $product->getStores()->first()->getStore() instanceof Store) {
			$nextStore = $product->getStores()->first()->getStore();
			return $this->removeProduct($product, $nextStore, $pluss);
		}
		return $pluss;
		
	}
	
	protected function removeStoreFromList(Store $store, $data)
	{
		foreach ($data as $key => $item){
			if($item->getId() == $store->getId()) {
				unset($data[$key]);
			}
		}
		return $data;
	}
	
	/**
	 * @param Product $product
	 * @param Store $store
	 * @return array
	 */
	protected function loadProductIntStore(Product $product, Store $store)
	{
		return $this->admin->getModelManager()->findBy('AppBundle\Entity\ProductInStore', ['product' => $product, 'store' => $store]);
	}
	
	/**
	 * @return array
	 */
	protected function getAllStore()
	{
		return $this->admin->getModelManager()->findBy('AppBundle\Entity\Store', []);
	}
	
}