<?php

namespace AppBundle\Model;

/**
 * @author Gabor Csatlos <gabor.csatlos86@gmail.com>
 */
interface ItemInterface {
	
	
	/**
	 * @return int
	 */
	public function getId();
	
	/**
	 * @param int $id
	 */
	public function setId($id);
	
	/**
	 * @return string
	 */
	public function __toString();
	
}