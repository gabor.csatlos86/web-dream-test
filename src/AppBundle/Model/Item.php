<?php

namespace AppBundle\Model;

/**
 * @author Gabor Csatlos <gabor.csatlos86@gmail.com>
 */
abstract class Item implements ItemInterface {
	
	/**
	 * @var int
	 */
	protected $id;
}